package org.sic.base;

import java.util.jar.Manifest;

import org.sic.util.JarVersion;

public class Version extends JarVersion {

    protected Version(final String packageName, final Manifest mf) {
        super(packageName, mf);
    }

    /**
     * Returns a transient new instance.
     */
    public static Version getInstance() {
        final String packageName = "org.sic.base";
        Manifest mf = JarVersion.getManifest(Version.class.getClassLoader(), packageName);
        if(null != mf) {
            return new Version(packageName, mf);
        } else {
            return null;
        }
    }

    public static void main(final String args[]) {
        final Version v = Version.getInstance();
        System.err.println("Version Info:");
        if( null != v ) {
        	System.err.println(v);
	        System.err.println("Full Manifest:");
	        System.err.println(v.getFullManifestInfo(null));
        } else {
            System.err.println("Couldn't retrieve version info.");
        }
    }

}
