package sic.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * Siehe Logik, JUC2 02.01 Logic01
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class Test0201Logik01Aufgabe02 {

	/**
	 * Returns the simplified term of:
	 * <ul>
	 *   <li>(¬A ∨ ¬B) ∧ (¬A ∨ B) ∧ (A ∨ ¬B)</li>
	 *   <li>(!a || !b) && (!a || b) && (a || !b)</li>
	 * </ul>
	 * Solution is ¬A ∨ ¬B
	 */
	static boolean term01(final boolean a, final boolean b) {
	    // Optimierung ist super, aber: Lesbarkeit versus Optimierung beachten!
	    //
		// 0: (!a || !b) && (!a || b) && (a || !b);
		// 1: ( !a || (!b && b) )     && (a || !b); // Distributivgesetze ('faktorisiert' -> !a rausgezogen)
		// 2: ( !a || false )         && (a || !b); // Neutralitaetsgesetze: !A ∨ 0 = !A
		// 3:   !a                    && (a || !b); // Distributivgesetze ('ausmultipliziert')
	    // 4: false || ( !a && !b );
	    return !a && !b;
	}

	/**
	 * Returns the simplified term of:
	 * <ul>
	 *   <li>(A ∧ B) ∨ (A ∧ C) ∨ (B ∧ ¬C)</li>
	 *   <li>(a && b) || (a && c) || (b && !c)</li>
	 * </ul>
	 */
	static boolean term02(final boolean a, final boolean b, final boolean c) {
		return (a && b) || (a && c) || (b && !c); // TODO: Simplify
	}

	/**
	 * Returns the simplified term of:
	 * <ul>
	 *   <li>(A ∧ ¬B) ∨ (A ∧ ¬B ∧ C)</li>
	 *   <li>(a && !b) || (a && !b && c)</li>
	 * </ul>
	 */
	static boolean term03(final boolean a, final boolean b, final boolean c) {
		return (a && !b) || (a && !b && c); // TODO: Simplify
	}

	/**
	 * Returns the simplified term of:
	 * <ul>
	 *   <li>(A ∨ ¬(B ∧ A)) ∧ (C ∨ (D ∨ C))</li>
	 *   <li>(a || !(b && a)) && (c || (d || c))</li>
	 * </ul>
	 */
	static boolean term04(final boolean a, final boolean b, final boolean c, final boolean d) {
		return (a || !(b && a)) && (c || (d || c)); // TODO: Simplify
	}

	/**
	 * Returns the simplified term of:
	 * <ul>
	 *   <li>(¬(A ∧ B) ∨ ¬C) ∧ (¬A ∨ B ∨ ¬C)</li>
	 *   <li>(!(a && b) || !c) && (!a || b || !c)</li>
	 * </ul>
	 */
	static boolean term05(final boolean a, final boolean b, final boolean c) {
		return (!(a && b) || !c) && (!a || b || !c); // TODO: Simplify
	}

	/**
	 * Returns the simplified term of:
	 * <ul>
	 *   <li>¬(¬(A ∧ B) ∨ C) ∨ (A ∧ C)</li>
	 *   <li>!(!(a && b) || c) || (a && c)</li>
	 * </ul>
	 */
	static boolean term06(final boolean a, final boolean b, final boolean c) {
		return !(!(a && b) || c) || (a && c); // TODO: Simplify
	}

	/**
	 * Returns the simplified term of:
	 * <ul>
	 *   <li>(A ∨ B) ∧ (¬A ∨ B) ∧ (A ∨ ¬B) ∧ (¬A ∨ ¬B)</li>
	 *   <li>(a || b) && (!a || b) && (a || !b) && (!a || !b)</li>
	 * </ul>
	 */
	static boolean term07(final boolean a, final boolean b) {
		return (a || b) && (!a || b) && (a || !b) && (!a || !b); // TODO: Simplify
	}

	/**
	 * Returns the simplified term of:
	 * <ul>
	 *   <li>A ∨ (¬B ∧ ¬(A ∨ ¬B ∨ C))</li>
	 *   <li>a || (!b && !(a || !b || c))</li>
	 * </ul>
	 */
	static boolean term08(final boolean a, final boolean b, final boolean c) {
		return a || (!b && !(a || !b || c)); // TODO: Simplify
	}

	@Test
	void test00() {
		// bool'sche variable, logik, wahr (true, 1) oder falsch (false, 0)
		final boolean T = true;  // immutable (konstante) booleans
		final boolean F = false; // ..

		Assertions.assertEquals(T, term01(F, F));
		Assertions.assertEquals(F, term01(F, T));
		Assertions.assertEquals(F, term01(T, F));
		Assertions.assertEquals(F, term01(T, T));

		Assertions.assertEquals(F, term02(F, F, F));
		Assertions.assertEquals(F, term02(F, F, T));
		Assertions.assertEquals(T, term02(F, T, F));
		Assertions.assertEquals(F, term02(F, T, T));
		Assertions.assertEquals(F, term02(T, F, F));
		Assertions.assertEquals(T, term02(T, F, T));
		Assertions.assertEquals(T, term02(T, T, F));
		Assertions.assertEquals(T, term02(T, T, T));

		Assertions.assertEquals(F, term03(F, F, F));
		Assertions.assertEquals(F, term03(F, F, T));
		Assertions.assertEquals(F, term03(F, T, F));
		Assertions.assertEquals(F, term03(F, T, T));
		Assertions.assertEquals(T, term03(T, F, F));
		Assertions.assertEquals(T, term03(T, F, T));
		Assertions.assertEquals(F, term03(T, T, F));
		Assertions.assertEquals(F, term03(T, T, T));

		Assertions.assertEquals(F, term04(F, F, F, F));
		Assertions.assertEquals(T, term04(F, F, F, T));
		Assertions.assertEquals(T, term04(F, F, T, F));
		Assertions.assertEquals(T, term04(F, F, T, T));
		Assertions.assertEquals(F, term04(F, T, F, F));
		Assertions.assertEquals(T, term04(F, T, F, T));
		Assertions.assertEquals(T, term04(F, T, T, F));
		Assertions.assertEquals(T, term04(F, T, T, T));
		Assertions.assertEquals(F, term04(T, F, F, F));
		Assertions.assertEquals(T, term04(T, F, F, T));
		Assertions.assertEquals(T, term04(T, F, T, F));
		Assertions.assertEquals(T, term04(T, F, T, T));
		Assertions.assertEquals(F, term04(T, T, F, F));
		Assertions.assertEquals(T, term04(T, T, F, T));
		Assertions.assertEquals(T, term04(T, T, T, F));
		Assertions.assertEquals(T, term04(T, T, T, T));

		Assertions.assertEquals(T, term05(F, F, F));
		Assertions.assertEquals(T, term05(F, F, T));
		Assertions.assertEquals(T, term05(F, T, F));
		Assertions.assertEquals(T, term05(F, T, T));
		Assertions.assertEquals(T, term05(T, F, F));
		Assertions.assertEquals(F, term05(T, F, T));
		Assertions.assertEquals(T, term05(T, T, F));
		Assertions.assertEquals(F, term05(T, T, T));

		Assertions.assertEquals(F, term06(F, F, F));
		Assertions.assertEquals(F, term06(F, F, T));
		Assertions.assertEquals(F, term06(F, T, F));
		Assertions.assertEquals(F, term06(F, T, T));
		Assertions.assertEquals(F, term06(T, F, F));
		Assertions.assertEquals(T, term06(T, F, T));
		Assertions.assertEquals(T, term06(T, T, F));
		Assertions.assertEquals(T, term06(T, T, T));

		Assertions.assertEquals(F, term07(F, F));
		Assertions.assertEquals(F, term07(F, T));
		Assertions.assertEquals(F, term07(T, F));
		Assertions.assertEquals(F, term07(T, T));

		Assertions.assertEquals(F, term08(F, F, F));
		Assertions.assertEquals(F, term08(F, F, T));
		Assertions.assertEquals(F, term08(F, T, F));
		Assertions.assertEquals(F, term08(F, T, T));
		Assertions.assertEquals(T, term08(T, F, F));
		Assertions.assertEquals(T, term08(T, F, T));
		Assertions.assertEquals(T, term08(T, T, F));
		Assertions.assertEquals(T, term08(T, T, T));
	}
}
