package sic.test.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * This is just a rudimentary launcher, ignoring `@Before*` `@After*` annotations.
 * <p>
 * Only '@Test' annotations are being considered.
 * </p>
 * Example:
 * <pre>
 cd juc2-java/java_basics_00/src/test/java
 javac -cp /usr/share/java/junit-jupiter-api.jar:. sic/test/util/SimpleJunit5Launcher.java
 javac -cp /usr/share/java/junit-jupiter-api.jar:. sic/test/Test0104Java01.java
 java -cp /usr/share/java/junit-jupiter-api.jar:. sic.test.Test0104Java01
 </pre>
 *
 * For regular junit5 launches, consider using
 * <ul>
 * <li>Console Launcher
 *     https://junit.org/junit5/docs/current/user-guide/#running-tests-console-launcher</li>
 *
 * <li>JUnit Platform Launcher API
 *    https://junit.org/junit5/docs/current/user-guide/#launcher-api-execution</li>
 * </ul>
 *
 */
public class SimpleJunit5Launcher {

    private static final String junitTestAnnotationClazzName = "org.junit.jupiter.api.Test";

    /**
     * Returns number of failed tests, launching all junit5 tests of given class.
     * @param testClazz junit5 class to be launched
     * @return true if no failure occured
     */
    public static boolean runTests(final Class<?> testClazz) {
        final Class<?> junitTestAnnotationClazz;
        try {
            junitTestAnnotationClazz = SimpleJunit5Launcher.class.getClassLoader().loadClass(junitTestAnnotationClazzName);
        } catch (final ClassNotFoundException e) {
            e.printStackTrace();
            return false;
        }

        Object testObj;
        try {
            testObj = testClazz.getDeclaredConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException
                | IllegalArgumentException | InvocationTargetException
                | NoSuchMethodException | SecurityException e) {
            e.printStackTrace();
            return false;
        }

        int testCount = 0;
        int failedTestCount = 0;
        final Method[] methods = testClazz.getDeclaredMethods();
        for(final Method m : methods) {
            final Annotation[] annotations = m.getDeclaredAnnotations();
            for(final Annotation a : annotations) {
                final Class<?> aClazz = a.getClass();
                if( junitTestAnnotationClazz.isAssignableFrom( aClazz ) ) {
                    ++testCount;
                    try {
                        m.setAccessible(true); // force accessible even if (package) private
                        m.invoke(testObj);
                    } catch (IllegalAccessException
                            | IllegalArgumentException e) {
                        e.printStackTrace();
                        ++failedTestCount;
                    } catch (final InvocationTargetException e) {
                        e.getTargetException().printStackTrace();
                        ++failedTestCount;
                    }
                }
            }
        }
        System.err.print(testClazz.getName()+": Failed tests "+failedTestCount+"/"+testCount);
        if( 0 == failedTestCount ) {
            System.err.println(" - All OK");
        } else {
            System.err.println(" - Error(s) occured.");
        }
        return 0 == failedTestCount;
    }

}
